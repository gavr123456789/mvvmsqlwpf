# MVVMSQLWPF

## Some SQL manual commands  
  
```sql
-- create table
CREATE TABLE workers (
    id INT PRIMARY KEY AUTO_INCREMENT,
    firstname VARCHAR(20) NOT NULL,
    lastname VARCHAR(20) NOT NULL,
    middlename VARCHAR(20) NOT NULL,
    birthdate DATE NOT NULL,
    sex ENUM('male', 'female'),
    departament_id int,
    FOREIGN KEY (departament_id) REFERENCES departaments(id)
);


CREATE TABLE departaments
(
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(20) NOT NULL,
    director_id INT NULL
);

-- create values 
INSERT workers (firstname, lastname, middlename, birthdate, sex, departament_id) 
VALUES ('Иван', 'Иванов', 'Иванович', '1973-08-22', 'male', 1),
       ('Михаил', 'Иванов', 'Иванович', '1974-08-22', 'male', 1),
       ('Константин', 'Иванов', 'Иванович', '1975-08-22', 'male', 2);

INSERT departaments (name, director_id) 
VALUES ('Разработка ПО', 1),
       ('Тестирование ПО', 2);

-- select all to 1 table
SELECT workers.id, workers.firstname, workers.lastname,  workers.middlename, workers.birthdate, workers.sex, departaments.name
FROM workers
JOIN departaments ON workers.DepartamentId = departaments.id;

-- update data 
UPDATE workers
SET id = 2, firstname = "Владислав", lastname = "Владислав", middlename = "Владислав", birthdate = "1975-08-22", sex = "male", departament_id = 2
WHERE id = 2;
```
