﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace MySQL.Model.Validation
{
    class NumbersOnlyRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            int someNumField;

            try
            {
                if (((string)value).Length > 0)
                    someNumField = Int32.Parse((string)value);
            }
            catch (Exception e)
            {
                return new ValidationResult(false, $"Допустимы только числа: {e.Message}");
            }

            return ValidationResult.ValidResult;
        }
    }
}
