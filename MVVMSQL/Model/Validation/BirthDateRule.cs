﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace MySQL.Model.Validation
{
    public class BirthDateRule : ValidationRule
    {

        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            DateTime date = new DateTime();

            try
            {
                if (((string)value).Length > 0)
                    date = DateTime.Parse((string)value);
            }
            catch (Exception e)
            {
                return new ValidationResult(false, $"Неверные данные или {e.Message}");
            }

            DateTime date2 = new DateTime(1900,1,1);
            int compare = DateTime.Compare(date, date2);
            bool toLow = compare < 0;
            

            if (toLow)
            {
                return new ValidationResult(false,
                  $"Дата должна быть больше 1900 г.");
            }
            return ValidationResult.ValidResult;
        }
    }
}
