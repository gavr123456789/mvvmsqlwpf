﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace MySQL.Model.Validation
{
    public class GenderRule : ValidationRule
    {

        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            string sex = "";

            try
            {
                if (((string)value).Length > 0)
                    sex = (string)value;
            }
            catch (Exception e)
            {
                return new ValidationResult(false, $"Неверные данные или {e.Message}");
            }

            if (sex != "male" && sex != "female")
            {
                return new ValidationResult(false,
                  "Пожалуйста введите значение female или male.");
            }

            return ValidationResult.ValidResult;
        }
    }
}
