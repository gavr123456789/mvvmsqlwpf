﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace MySQL.Model.Validation
{
    public class NameRule : ValidationRule
    {

        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            string name = "";

            try
            {
                if (((string)value).Length > 0)
                    name = (string)value;
            }
            catch (Exception e)
            {
                return new ValidationResult(false, $"Неверные данные или {e.Message}");
            }

            if (name.Any(char.IsDigit))
            {
                return new ValidationResult(false,
                  "Строка не должна содержать цифры.");
            }
            return ValidationResult.ValidResult;
        }
    }
}
