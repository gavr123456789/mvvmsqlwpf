﻿using System;
using System.Windows;

namespace MySQL
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {

        protected override void OnStartup (StartupEventArgs e)
        {
            base.OnStartup(e);

            Current.MainWindow = new MainWindow();
            Current.MainWindow.Show();

            AppDomain.CurrentDomain.UnhandledException += (sender, ee) =>
            {
                Exception ex = ee.ExceptionObject as Exception;

                UnhandledExceptionMessage (ex.Source, 
                                           ex.TargetSite.ToString(), 
                                           ex.GetType().ToString(), 
                                           ex.Message, 
                                           ex.ToString(), 
                                           "Unhandled Non UIThread Exception");

                Current.MainWindow.Close();
            };

            DispatcherUnhandledException += (sender, ee) =>
            {
                UnhandledExceptionMessage (ee.Exception.Source, 
                                           ee.Exception.TargetSite.ToString(), 
                                           ee.Exception.GetType().ToString(), 
                                           ee.Exception.Message,
                                           ee.Exception.ToString(), 
                                           "Unhandled Main UIThread Exception");

                Current.MainWindow.Close();
            };

        }       

        private void UnhandledExceptionMessage(string source, string function, string type, string message, string stackTrace, string title)
        {
            MessageBox.Show("Namespace: \n   " + source +
                            "\n\nFunction: \n   " + function +
                            "\n\nType: \n   " + type +
                            "\n\nMessage: \n   " + message +
                            "\n\nStackTrace: \n   " + stackTrace
                           , title, MessageBoxButton.OK, MessageBoxImage.Error);
        }
    }
}
