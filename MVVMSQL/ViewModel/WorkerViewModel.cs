﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using MySql.Data.MySqlClient;
using MySQL.ViewModel.Base;

namespace MySQL
{

    public class WorkerViewModel : BaseDbViewModel
    {
        #region Private Properties

        private object ClassLock = new object();

        #endregion

        #region Public Properties

        //Бойлерплейт для MVVM
        public ObservableCollection<Worker> WorkerClass { get; set; }  = new ObservableCollection<Worker>();
        public int WorkerClassSelectedIndex { get; set; }
        public object WorkerClassSelectedItem { get; set; }

        //Поля формы
        public string ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Birthdate { get; set; }
        public string Sex { get; set; }
        public string Departmentid { get; set; }

        #endregion

        #region Commands
        public ICommand WorkResetCommand { get; set; }
        public ICommand WorkInsertCommand { get; set; }
        public ICommand WorkSearchCommand { get; set; }
        public ICommand WorkUpdateCommand { get; set; }
        public ICommand WorkDeleteCommand { get; set; }
        #endregion

        #region Constructor

        public WorkerViewModel()
        {
            BindingOperations.CollectionRegistering += (sender, e) =>
            {
                if (e.Collection == WorkerClass)
                {
                    BindingOperations.EnableCollectionSynchronization (WorkerClass, ClassLock);
                }
            };
            
            // Первое чтение из БД в таблички
            ReadWorkerDataAsync (ReadingType.Inserting);
            
            WorkResetCommand = new RelayCommand (() => ResetWorkerForm());

            WorkInsertCommand = new RelayCommand (async () => await InsertWorkerAsync());
            WorkSearchCommand = new RelayCommand (async () => await SearchDataAsync());
            WorkUpdateCommand = new RelayCommand (async () => await UpdateDataAsync());
            WorkDeleteCommand = new RelayCommand (async () => await DeleteDataAsync());
        }

        #endregion

        #region Items management

        private void GetFocus (ReadingType dataBaseAction)
        {
            if (WorkerClass.Count == 0)
                return;

            switch (dataBaseAction)
            {
                case ReadingType.Inserting:
                    WorkerClassSelectedIndex = WorkerClass.Count - 1;
                    break;
                default:
                    WorkerClassSelectedIndex = FindIndexfromId (FocusDict[dataBaseAction]);
                    break;
            }
        }

        private int FindIndexfromId (int id)
        {
            for (int i = 0; i < WorkerClass.Count; i++)
                if ((WorkerClass[i]?.ID ?? -1) == id)
                    return i;

            return -1;
        }

        private int FindIdfromIndex (int index)
        {
            if (index >= WorkerClass.Count)
                return -1;

            return WorkerClass[index]?.ID ?? -1;
        }

        private void ResetWorkerForm ()
        {
            ID =            "";
            FirstName =     "";
            LastName =      "";
            MiddleName =    "";
            Birthdate =     "";
            Sex =           "";
            Departmentid =  "";
        }

        private Status SetWorkerForm (string id,
                                      string firstName,
                                      string lastName,
                                      string middleName,
                                      string birthDate,
                                      string sex,
                                      string departamentId)
        {
            if (IsNotNullFields(id, firstName, lastName, middleName, birthDate, sex, departamentId) == false)
                return Status.Failed;

            else
            {
                ID = id;
                FirstName = firstName;
                LastName = lastName;
                MiddleName = middleName;
                Birthdate = birthDate;
                Sex = sex;
                Departmentid = departamentId;

                return Status.Successful;
            }
        }

        private (Status, Worker) GetWorkerFromId (int id)
        {
            Status status = Status.Failed;
            Worker worker = new Worker();

            if (WorkerClass.Count == 0) return (status, worker);

            for (int i = 0; i < WorkerClass.Count; i++)
            {
                if ((WorkerClass[i]?.ID ?? -1) == id)
                {
                    worker.ID =            WorkerClass[i]?.ID ?? -1;
                    worker.FirstName =     WorkerClass[i]?.FirstName ?? "";
                    worker.LastName =      WorkerClass[i]?.LastName ?? "";
                    worker.MiddleName =    WorkerClass[i]?.MiddleName ?? "";
                    worker.Birthdate =     WorkerClass[i]?.Birthdate ?? new DateTime();
                    worker.Sex =           WorkerClass[i]?.Sex ?? "";
                    worker.DepartamentId = WorkerClass[i]?.DepartamentId ?? -1;
                    break;
                }
            }

            // Check if the value are valid
            if (worker.ID == -1 ||
                worker.DepartamentId == -1 ||
                IsNotNullFields(worker.FirstName,
                                worker.LastName,
                                worker.MiddleName,
                                worker.Birthdate.ToString("yyyy-MM-dd"),
                                worker.Sex) == false)
            {
                status = Status.Failed;
            }
            else
            {
                status = Status.Successful;
            }

            return (status, worker);
        }

        private (Status, Worker) GetWorkerFromIndex (int index)
        {
            Status status = Status.Failed;
            Worker worker = new Worker();

            if (index < 0 && index >= WorkerClass.Count) return (status, worker);

            worker.ID =                WorkerClass[index]?.ID ?? -1;
            worker.FirstName =         WorkerClass[index]?.FirstName ?? "";
            worker.LastName =          WorkerClass[index]?.LastName ?? "";
            worker.MiddleName =        WorkerClass[index]?.MiddleName ?? "";
            worker.Birthdate = WorkerClass[index]?.Birthdate ?? new DateTime();
            worker.Sex =               WorkerClass[index]?.Sex ?? "";
            worker.DepartamentId =     WorkerClass[index]?.DepartamentId ?? -1;

            if (worker.ID == -1 ||
                worker.DepartamentId == -1 ||
                IsNotNullFields(worker.FirstName,
                                worker.LastName,
                                worker.MiddleName,
                                worker.Birthdate.ToString("yyyy-MM-dd"),
                                worker.Sex) == false)
            {
                status = Status.Failed;
            }
            else
            {
                status = Status.Successful;
            }

            return (status, worker);
        }

        bool WorkerValidation (string firstName, string lastName, string middlename, string birthDate, string sex, string departamentId)
        {
            if (IsNotNullFields(firstName, lastName, middlename, birthDate, sex, departamentId) == false)
                return false;

            if (IsAllLetters(firstName) &&
                IsAllLetters(lastName) &&
                IsAllLetters(middlename) &&
                IsDateTime(birthDate) &&
                IsAllDigits(departamentId)) 

                return true;
            else 
                return false;
        }

        #endregion

        #region Read data worker

        private async void ReadWorkerDataAsync (ReadingType dataBaseAction)
        {            
            var (status, tempClasse) = await Task.Run(() => ReadWorkerData());

            if (status == Status.Successful)
            {
                // Clear the list of workers
                WorkerClass.Clear();

                // Update the list of workers
                tempClasse.ForEach (val => WorkerClass.Add(val));
            }

            GetFocus (dataBaseAction);
        }

        private (Status, List<Worker>) ReadWorkerData()
        {
            string query = "SELECT * FROM mydb.workers";

            var (status, tempClasse) = ReadWorkerRequest (query, "Error: database reading");
            return (status, tempClasse);
        }

        private (Status, List<Worker>) ReadWorkerRequest(string query, string errorMessageTitle)
        {
            Status status = Status.Failed;
            
            List<Worker> tempWorkers = new List<Worker>();
            try
            {
                using (MySqlConnection connection = new MySqlConnection(Database.ConnectionString))
                {
                    connection.Open();
                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        // Execute 
                        using (MySqlDataReader mysqlReader = command.ExecuteReader())
                        {
                            while (mysqlReader.Read() == true)
                            {
                                tempWorkers.Add(new Worker()
                                {
                                    ID = mysqlReader.GetInt16(0),
                                    FirstName = mysqlReader.GetString(1),
                                    LastName = mysqlReader.GetString(2),
                                    MiddleName = mysqlReader.GetString(3),
                                    Birthdate = Convert.ToDateTime(mysqlReader.GetString(4)),
                                    Sex = mysqlReader.GetString(5),
                                    DepartamentId = mysqlReader.GetInt16(6)
                                });
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, errorMessageTitle, MessageBoxButton.OK, MessageBoxImage.Error);
            }

            status = (tempWorkers.Count > 0) ? Status.Successful : Status.Failed;
            return (status, tempWorkers);
        }

        #endregion

        #region Worker sql methods 

        #region Insert data 

        private async Task InsertWorkerAsync ()
        {
            var status = await Task.Run(() => InsertWorkerData());

            if (status == Status.Successful)
                ReadWorkerDataAsync (ReadingType.Inserting);
        }

        private Status InsertWorkerData()
        {
            Status status = Status.Failed;

            string firstName = FirstName?.Trim();
            string lastName = LastName?.Trim();
            string middleName = MiddleName?.Trim();
            string birthDate= Birthdate?.Trim();
            string sex = Sex?.Trim();
            string departamentId = Departmentid?.Trim();

            if (WorkerValidation(firstName, lastName, middleName, birthDate, sex, departamentId) == false)
                return status;
            
            string query = "INSERT INTO mydb.workers(firstname, lastname, middlename, birthdate, sex, departament_id) " +
                           "VALUES(@firstName, @lastName, @middleName, @birthDate, @sex, @departamentId) ";
            
            int affectedRows = 0;

            try
            {
                using (MySqlConnection connection = new MySqlConnection(Database.ConnectionString))
                {
                    
                    connection.Open();

                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        string[] strValues = { "@firstName", "@lastName", "@middleName", "@birthDate", "@sex", "@departamentId" };
                        string[] values = { firstName, lastName, middleName, birthDate, sex, departamentId };
                        ParamInjection(command, strValues, values);
                        // Execute  
                        affectedRows = command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error: database inserting", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return (affectedRows > 0) ? Status.Successful : Status.Failed;
            ///

        }

        #endregion

        #region Search data

        private async Task SearchDataAsync()
        {
            ResetWorkerForm();

            var (status, worker) = await Task.Run(() => SearchData());

            if (status == Status.Successful)
            {
                status = SetWorkerForm(worker.ID.ToString(),
                                         worker.FirstName,
                                         worker.LastName,
                                         worker.MiddleName,
                                         worker.Birthdate.ToString("yyyy-MM-dd"),
                                         worker.Sex,
                                         worker.DepartamentId.ToString());

                // Update current ID
                FocusDict[ReadingType.Searching] = worker.ID;
                GetFocus(ReadingType.Searching);
            }
        }

        private (Status, Worker) SearchData()
        {
            Status status = Status.Failed;
            Worker worker = new Worker();

            string id = ID?.Trim();

            if (string.IsNullOrWhiteSpace(id) == true)
            {
                var index = WorkerClassSelectedIndex;
                id = FindIdfromIndex(index).ToString();
            }

            if (string.IsNullOrWhiteSpace(id) == true || !IsAllDigits(id)) return (status, worker);

            string query = "SELECT * FROM mydb.workers WHERE id = @id";
            (status, worker) = SearchRequest(query, "Error: database searching", id);

            return (status, worker);
        }

        private (Status, Worker) SearchRequest(string query, string errorMessageTitle, string id)
        {
            Status status = Status.Failed;
            Worker worker = new Worker();

            try
            {
                using (MySqlConnection connection = new MySqlConnection(Database.ConnectionString))
                {

                    connection.Open();

                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        string[] strValues = { "@id" };
                        string[] values = { id };
                        ParamInjection(command, strValues, values);
                        // Execute  
                        using (MySqlDataReader mysqlReader = command.ExecuteReader())
                        {
                            if (mysqlReader.Read() == true)
                            {
                                worker.ID = mysqlReader.GetInt16(0);
                                worker.FirstName = mysqlReader.GetString(1);
                                worker.LastName = mysqlReader.GetString(2);
                                worker.MiddleName = mysqlReader.GetString(3);
                                worker.Birthdate = Convert.ToDateTime(mysqlReader.GetString(4));
                                worker.Sex = mysqlReader.GetString(5);
                                worker.DepartamentId = mysqlReader.GetInt32(6);

                                status = Status.Successful;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, errorMessageTitle, MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return (status, worker);
        }

        #endregion

        #region Update data

        private async Task UpdateDataAsync ()
        {                    
            var (status, id) = await Task.Run(() => UpdateData());

            if (status == Status.Successful)
            {
                FocusDict[ReadingType.Updating] = id.ParseInt();
                ReadWorkerDataAsync (ReadingType.Updating);
            }            
        }

        private (Status, string) UpdateData()
        {
            Status status = Status.Failed;
            
            string firstName = FirstName?.Trim();
            string lastName = LastName?.Trim();
            string middleName = MiddleName?.Trim();
            string birthDate = Birthdate?.Trim();
            string sex = Sex?.Trim();
            string departamentId = Departmentid?.Trim();

            
            // Current selected index
            var index = WorkerClassSelectedIndex;
            string id = FindIdfromIndex(index).ToString();

            if (WorkerValidation(firstName, lastName, middleName, birthDate, sex, departamentId) == false)
                return (status, id);

            string query = "UPDATE mydb.workers SET firstName = @firstName, " +
                                                    "lastname = @lastName, " +
                                                    "middlename = @middleName, " +
                                                    "birthdate = @birthDate, " +
                                                    "sex = @sex, " +
                                                    "departament_id = @departamentId " +
                                                    "WHERE id = @id" ;
            int affectedRows = 0;

            try
            {
                using (MySqlConnection connection = new MySqlConnection(Database.ConnectionString))
                {
                    connection.Open();
                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        string[] strValues = { "@firstName", "@lastName", "@middleName", "@birthDate", "@sex", "@departamentId", "@id" };
                        string[] values = { firstName, lastName, middleName, birthDate, sex, departamentId, id };
                        ParamInjection(command, strValues, values);
                        // Execute  
                        affectedRows = command.ExecuteNonQuery(); // только для вставки обновления и удаления
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error: database updating", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            status = (affectedRows > 0) ? Status.Successful : Status.Failed;
            return (status, id);
        }
        #endregion

        #region Delete data

        private async Task DeleteDataAsync ()
        {
            var (status, worker) = await Task.Run(() => DeleteData ());

            if (status == Status.Successful)
            {
                status = SetWorkerForm (worker.ID.ToString(),
                                         worker.FirstName,
                                         worker.LastName,
                                         worker.MiddleName,
                                         worker.Birthdate.ToString("yyyy-MM-dd"),
                                         worker.Sex,
                                         worker.DepartamentId.ToString());

                var index = FindIndexfromId (worker.ID);

                var id = FindIdfromIndex (index + 1);

                // Current selected index
                FocusDict[ReadingType.Deleting] = id;
                ReadWorkerDataAsync (ReadingType.Deleting);
            } 
        }

        private (Status, Worker) DeleteData ()
        {            
            Status status = Status.Failed;
            Worker worker = new Worker();

            // Get current selected index 
            int index = WorkerClassSelectedIndex;

            int id = FindIdfromIndex (WorkerClassSelectedIndex);
            if (id < 0) return (status, worker);

            string query = "DELETE FROM mydb.workers WHERE id = @id";

            int affectedRows = 0;

            try
            {
                using (MySqlConnection connection = new MySqlConnection(Database.ConnectionString))
                {
                    connection.Open();

                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        string[] strValues = { "@id" };
                        string[] values = { id.ToString() };
                        ParamInjection(command, strValues, values);
                        // Execute 
                        affectedRows = command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error: database deleting", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            status = (affectedRows > 0) ? Status.Successful : Status.Failed;

            if (status == Status.Failed) return (status, worker);

            // Get deleted worker 
            (status, worker) = GetWorkerFromIndex (index);

            return (status, worker);
        }

        #endregion

        #endregion
    }

    enum SexType { Male, Female };  

    public class Worker: BaseViewModel
    {
        public int ID { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }

        public DateTime Birthdate { get; set; }
        public string Sex { get; set; }

        public int DepartamentId { get; set; }
    }


}
