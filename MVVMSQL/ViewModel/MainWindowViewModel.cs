﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MySQL.ViewModel
{
    class MainWindowViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public DepartmentViewModel departmentsViewModel { get; set; } 
        public WorkerViewModel workersViewModel { get; set; }

        public MainWindowViewModel()
        {
            workersViewModel = new WorkerViewModel();
            departmentsViewModel = new DepartmentViewModel();
        }
    
    }
}
