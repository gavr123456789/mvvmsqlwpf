﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using MySql.Data.MySqlClient;
using MySQL.ViewModel.Base;

namespace MySQL.ViewModel
{
    class DepartmentViewModel : BaseDbViewModel
    {
        #region Private Properties
        private object ClassLock = new object();
        private MainWindow mainWindow;
        #endregion

        #region Public Properties
        public ObservableCollection<Department> DepartmentClass { get; set; } = new ObservableCollection<Department>();
        public int DepartmentClassSelectedIndex { get; set; }
        public object DepartmentClassSelectedItem { get; set; }

        public string DepID { get; set; }
        public string DepName { get; set; }
        public string DirectorId { get; set; }
        #endregion

        #region Commands
        public ICommand DepResetCommand  { get; set; }
        public ICommand DepInsertCommand { get; set; }
        public ICommand DepSearchCommand { get; set; }
        public ICommand DepUpdateCommand { get; set; }
        public ICommand DepDeleteCommand { get; set; }
        #endregion

        #region Constructor
        public DepartmentViewModel()
        {
            BindingOperations.CollectionRegistering += (sender, e) =>
            {
                if (e.Collection == DepartmentClass)
                {
                    BindingOperations.EnableCollectionSynchronization(DepartmentClass, ClassLock);
                }
            };

            // Первое чтение из БД в таблички
            ReadDepartmentDataAsync(ReadingType.Inserting);

            DepResetCommand = new RelayCommand(() => ResetDepartmentForm());
            DepInsertCommand = new RelayCommand(async () => await InsertDepartmentAsync());
            DepUpdateCommand = new RelayCommand(async () => await UpdateDataAsync());
            DepSearchCommand = new RelayCommand(async () => await SearchDataAsync());
            DepDeleteCommand = new RelayCommand(async () => await DeleteDataAsync());
        }

        public DepartmentViewModel(MainWindow mainWindow)
        {
            this.mainWindow = mainWindow;
        }
        #endregion

        #region Items management
        private void ResetDepartmentForm()
        {
            DepID = "";
            DepName = "";
            DirectorId = "";
        }

        bool DepartmentValidation(string depName, string directorId)
        {
            if (IsNotNullFields(depName, directorId) == false)
                return false;

            if (IsAllLetters(depName) && IsAllDigits(directorId)) 
                return true;
            else return false;
        }


        private void GetFocus(ReadingType dataBaseAction)
        {
            if (DepartmentClass.Count == 0)
                return;
            // Set selected index 
            switch (dataBaseAction)
            {
                case ReadingType.Inserting:
                    DepartmentClassSelectedIndex = DepartmentClass.Count - 1;
                    break;
                default:
                    DepartmentClassSelectedIndex = FindIndexfromId(FocusDict[dataBaseAction]);
                    break;
            }
        }

        private int FindIndexfromId(int id)
        {
            for (int i = 0; i < DepartmentClass.Count; i++)
                if ((DepartmentClass[i]?.DepID ?? -1) == id)
                    return i;

            return -1;
        }

        private int FindIdfromIndex(int index)
        {
            if (index >= DepartmentClass.Count)
                return -1;

            return DepartmentClass[index]?.DepID ?? -1;
        }

        private Status SetDepartmentForm(string depId,
                              string depName,
                              string directorId)
        {
            if (IsNotNullFields(depId, depName, directorId) == false)
                return Status.Failed;

            else
            {
                DepID = depId;
                DepName = depName;
                DirectorId = directorId;

                return Status.Successful;
            }
        }

        private (Status, Department) GetWorkerFromIndex(int index)
        {
            Status status = Status.Failed;
            Department department = new Department();

            if (index < 0 && index >= DepartmentClass.Count) return (status, department);

            department.DepID = DepartmentClass[index]?.DepID ?? -1;
            department.DepName = DepartmentClass[index]?.DepName ?? "";
            department.DirectorId = DepartmentClass[index]?.DirectorId ?? -1;
           

            if (department.DepID == -1 ||
                department.DirectorId == -1 ||
                IsNotNullFields(department.DepName) == false)
            {
                status = Status.Failed;
            }
            else
            {
                status = Status.Successful;
            }

            return (status, department);
        }

        #endregion

        #region Insert data 
        private async Task InsertDepartmentAsync()
        {
            var status = await Task.Run(() => InsertDepartmentData());

            if (status == Status.Successful)
                ReadDepartmentDataAsync(ReadingType.Inserting);
            
        }

        private Status InsertDepartmentData()
        {
            Status status = Status.Failed;
            string name = DepName?.Trim();
            string directorId = DirectorId?.Trim();

            if (DepartmentValidation(name, directorId) == false)
                return status;

            // Format the insert request
            string query = "INSERT INTO mydb.departaments(name, director_id) " +
                           "VALUES(@name, @directorId)";

            
            int affectedRows = 0;

            try
            {
                using (MySqlConnection connection = new MySqlConnection(Database.ConnectionString))
                {
                    connection.Open();

                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        string[] strValues = { "@name", "@directorId" };
                        string[] values = { name, directorId };
                        ParamInjection(command, strValues, values);
                        // Execute
                        affectedRows = command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error: database inserting", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return (affectedRows > 0) ? Status.Successful : Status.Failed;
        }


        #endregion

        #region Read data department
        private async void ReadDepartmentDataAsync(ReadingType dataBaseAction)
        {
            // Update department array 
            var (status, tempDepartments) = await Task.Run(() => ReadDepartmentData());

            if (status == Status.Successful)
            {
                DepartmentClass.Clear();
                // Update the list of deps
                tempDepartments.ForEach(val => DepartmentClass.Add(val));
            }

            GetFocus(dataBaseAction);
        }

        private (Status, List<Department>) ReadDepartmentData()
        {
            // Format the select request
            string query = "SELECT * FROM mydb.departaments";

            // Database read request 
            var (status, tempClasse) = ReadDepartmentRequest(query, "Error: database reading");
            return (status, tempClasse);
        }

        private (Status, List<Department>) ReadDepartmentRequest(string query, string errorMessageTitle)
        {
            Status status;

            var tempWorkers = new List<Department>();
            try
            {
                using (var connection = new MySqlConnection(Database.ConnectionString))
                {
                    // Connection opening
                    connection.Open();
                    using (var command = new MySqlCommand(query, connection))
                    {
                        // Execute the command 
                        using (var mysqlReader = command.ExecuteReader())
                        {
                            // Check if there is any row left from the executed command  
                            while (mysqlReader.Read() == true)
                            {
                                tempWorkers.Add(new Department()
                                {
                                    DepID = mysqlReader.GetInt16(0),
                                    DepName = mysqlReader.GetString(1),
                                    DirectorId = mysqlReader.GetInt16(2)
                                });
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, errorMessageTitle, MessageBoxButton.OK, MessageBoxImage.Error);
            }

            status = (tempWorkers.Count > 0) ? Status.Successful : Status.Failed;
            return (status, tempWorkers);
        }
        #endregion

        #region Update data

        private async Task UpdateDataAsync()
        {
            var (status, id) = await Task.Run(() => UpdateData());

            if (status == Status.Successful)
            {
                // Update current ID
                FocusDict[ReadingType.Updating] = id.ParseInt();

                ReadDepartmentDataAsync(ReadingType.Updating);
            }
        }

        private (Status, string) UpdateData()
        {
            Status status = Status.Failed;

            string depName = DepName?.Trim();
            string directorId = DirectorId?.Trim();

            // Current selected index
            var index = DepartmentClassSelectedIndex;
            string depId = FindIdfromIndex(index).ToString();


            if (DepartmentValidation(depName, directorId) == false)
                return (status, depId);

            string query = "UPDATE mydb.departaments SET name = @depName, " +
                                                    "director_id = @directorId " +
                                                    "WHERE id = @depId";
            // DB update
            int affectedRows = 0;

            try
            {
                using (MySqlConnection connection = new MySqlConnection(Database.ConnectionString))
                {
                    connection.Open();
                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        string[] strValues = { "@depName", "@directorId", "@depId" };
                        string[] values = { depName, directorId, depId };
                        ParamInjection(command, strValues, values);

                        affectedRows = command.ExecuteNonQuery(); 
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error: database updating", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            status = (affectedRows > 0) ? Status.Successful : Status.Failed;
            return (status, depId);
        }
        #endregion

        #region Search data

        private async Task SearchDataAsync()
        {
            // Reset department form
            ResetDepartmentForm();

            var (status, department) = await Task.Run(() => SearchData());

            if (status == Status.Successful)
            {
                // Set the department form
                status = SetDepartmentForm(department.DepID.ToString(),
                                           department.DepName,
                                           department.DirectorId.ToString()
                                          );

                // Update current ID
                FocusDict[ReadingType.Searching] = department.DepID;

                // Get focus to the appropriete item
                GetFocus(ReadingType.Searching);
            }
        }

        private (Status, Department) SearchData()
        {
            Status status = Status.Failed;
            Department deportment = new Department();
            string id = DepID?.Trim();

            if (string.IsNullOrWhiteSpace(id) == true)
            {
                // Current selected index
                var index = DepartmentClassSelectedIndex;
                id = FindIdfromIndex(index).ToString();
            }

            if (string.IsNullOrWhiteSpace(id) == true || !IsAllDigits(id)) 
                return (status, deportment);

            string query = "SELECT * FROM mydb.departaments WHERE id = @id";

            (status, deportment) = SearchRequest(query, "Error: database searching", id/*, Database.SearchingType.NoDatabase*/);
            return (status, deportment);
        }

        private (Status, Department) SearchRequest(string query, string errorMessageTitle, string id/*, Database.SearchingType type*/)
        {
            Status status = Status.Failed;
            Department department = new Department();

            try
            {
                using (MySqlConnection connection = new MySqlConnection(Database.ConnectionString))
                {
                    connection.Open();

                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        string[] strValues = { "@id" };
                        string[] values = { id };
                        ParamInjection(command, strValues, values);

                        using (MySqlDataReader mysqlReader = command.ExecuteReader())
                        {
                            if (mysqlReader.Read() == true)
                            {
                                department.DepID = mysqlReader.GetInt16(0);
                                department.DepName = mysqlReader.GetString(1);
                                department.DirectorId = mysqlReader.GetInt16(2);

                                status = Status.Successful;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, errorMessageTitle, MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return (status, department);
        }

        #endregion

        #region Delete data

        private async Task DeleteDataAsync()
        {
            var (status, department) = await Task.Run(() => DeleteData());

            if (status == Status.Successful)
            {
                status = SetDepartmentForm(department.DepID.ToString(),
                                           department.DepName,
                                           department.DirectorId.ToString()
                                          );

                var index = FindIndexfromId(department.DepID);

                // Select next id
                var id = FindIdfromIndex(index + 1);
                FocusDict[ReadingType.Deleting] = id;
                ReadDepartmentDataAsync(ReadingType.Deleting);
            }
        }

        private (Status, Department) DeleteData()
        {
            Status status = Status.Failed;
            Department department = new Department();

            int index = DepartmentClassSelectedIndex;

            int id = FindIdfromIndex(DepartmentClassSelectedIndex);
            if (id < 0) return (status, department);

            string query = "DELETE FROM mydb.departaments WHERE id = @depId";

            int affectedRows = 0;

            try
            {
                using (MySqlConnection connection = new MySqlConnection(Database.ConnectionString))
                {
                    connection.Open();

                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        string[] strValues = { "@depId" };
                        string[] values = { id.ToString() };
                        ParamInjection(command, strValues, values);
                        // Execute 
                        affectedRows = command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error: database deleting", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            status = (affectedRows > 0) ? Status.Successful : Status.Failed;

            if (status == Status.Failed) return (status, department);

            // Get deleted department 
            (status, department) = GetWorkerFromIndex(index);

            return (status, department);
        }

        #endregion
    }

    public class Department : BaseViewModel
    {
        public int DepID { get; set; }
        public string DepName { get; set; }
        public int DirectorId { get; set; }
    }
}
