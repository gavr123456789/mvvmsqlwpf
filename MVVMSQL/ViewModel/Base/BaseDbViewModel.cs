﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MySQL.ViewModel.Base
{
    public abstract class BaseDbViewModel : BaseViewModel
    {
        protected enum ReadingType { Inserting, Searching, Updating, Deleting };
        protected Dictionary<ReadingType, int> FocusDict = new Dictionary<ReadingType, int>  {{ ReadingType.Searching, -1 },
                                                                                            { ReadingType.Updating, -1 },
                                                                                            { ReadingType.Deleting, -1 }};
        // For validation
        protected bool IsAllDigits(string s) => s.All(char.IsDigit);
        protected bool IsAllLetters(string s) => Regex.IsMatch(s, @"^[\p{L} ]+$");
        protected bool IsDateTime(string s) => DateTime.TryParse(s, out var temp);
        protected bool IsNotNullFields(params string[] values)
        {
            foreach (var item in values)
                if (string.IsNullOrWhiteSpace(item))
                    return false;

            return true;
        }

        //SQL-injection fix
        protected void ParamInjection(MySql.Data.MySqlClient.MySqlCommand command, string[] strValues, string[] values)
        {
            Contract.Requires(strValues.Length == values.Length, "Количество строковых аргументов и значений должно совпадать");
            for (int i = 0; i < strValues.Length; i++)
            {
                command.Parameters.AddWithValue(strValues[i], values[i]);
            }
            command.Prepare();
        }

        
    }
}
