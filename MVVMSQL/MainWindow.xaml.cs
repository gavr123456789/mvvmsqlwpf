﻿using MahApps.Metro.Controls;
using MySQL.ViewModel;
using System.Windows;

namespace MySQL
{  
    public partial class MainWindow : Window
    {        
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new MainWindowViewModel ();

        }
    }    
}
